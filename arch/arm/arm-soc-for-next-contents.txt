fixes
	<no branch> (44361a2cc13493fc41216d33bb9a562ec3a9cc4e)
		git://git.infradead.org/linux-mvebu tags/mvebu-fixes-4.5-2
	<no branch> (cf26f1137333251f3515dea31f95775b99df0fd5)
		git://git.kernel.org/pub/scm/linux/kernel/git/tmlind/linux-omap tags/omap-for-v4.5/fixes-rc3-v2
	<no branch> (f5d0ca224a071d7077bbdae347cb514949d64fd9)
		git://git.kernel.org/pub/scm/linux/kernel/git/shawnguo/linux tags/imx-fixes-4.5
	patch
		ARM: at91/dt: fix typo in sama5d2 pinmux descriptions
		MAINTAINERS: alpine: add a new maintainer and update the entry
	<no branch> (3f315c5b850fa7aff73f50de8e316b98f611a32b)
		git://git.kernel.org/pub/scm/linux/kernel/git/tmlind/linux-omap tags/omap-for-v4.5/fixes-rc5
	patch
		MAINTAINERS: Extend info, add wiki and ml for meson arch
	<no branch> (901c5ffaaed117a38be9d0c29247c4888d6c8636)
		git://git.kernel.org/pub/scm/linux/kernel/git/horms/renesas tags/renesas-soc-fixes-for-v4.5

next/fixes-non-critical
	smasung/fixes-nc
		git://git.kernel.org/pub/scm/linux/kernel/git/krzk/linux tags/samsung-fixes-non-critical-4.6
	patch
		ARM: ks8695: fix __initdata annotation
		ARM: ux500: fix ureachable iounmap()
		ARM: socfpga: hide unused functions
		ARM: prima2: always enable reset controller
	davinci/fixes
		git://git.kernel.org/pub/scm/linux/kernel/git/nsekhar/linux-davinci tags/davinci-for-v4.6/fixes
	patch
		soc: TI knav_qmss: fix dma_addr_t printing

next/cleanup
	patch
		ARM: drop unused Makefile.boot of Multiplatform SoCs
		ARM: integrator: remove redundant select in Kconfig
		ARM: netx: remove redundant "depends on ARCH_NETX"
	renesas/cleanup
		git://git.kernel.org/pub/scm/linux/kernel/git/horms/renesas tags/renesas-cleanup-for-v4.6
	mvebu/cleanup
		git://git.infradead.org/linux-mvebu tags/mvebu-cleanup-4.6-1
	lpc32xx/soc
		https://github.com/vzapolskiy/linux lpc32xx/soc
	versatile/cleanup
		git://git.kernel.org/pub/scm/linux/kernel/git/linusw/linux-integrator tags/plat-versatile-cleanup
	renesas/cleanup2
		git://git.kernel.org/pub/scm/linux/kernel/git/horms/renesas tags/renesas-cleanup2-for-v4.6

next/soc
	patch
		ARM: debug: add support for Palmchip BK-310x UART
	mvebu/drivers
		git://git.infradead.org/linux-mvebu tags/mvebu-drivers-4.6-1
	stm32/soc
		git://git.kernel.org/pub/scm/linux/kernel/git/mcoquelin/stm32 tags/stm32-soc-for-v4.6-1
	patch
		arm: initial machine port for artpec-6 SoC
		ARM: mach-artpec: add entry to MAINTAINERS
	mediatek/soc
		https://github.com/mbgg/linux-mediatek tags/v4.5-next-soc
	renesas/soc
		git://git.kernel.org/pub/scm/linux/kernel/git/horms/renesas tags/renesas-soc-for-v4.6
	davinci/edma
		git://git.kernel.org/pub/scm/linux/kernel/git/nsekhar/linux-davinci tags/davinci-for-v4.6/edma
	zynq/soc
		https://github.com/Xilinx/linux-xlnx tags/zynq-soc-for-4.6
	patch
		ARM: alpine: select the Alpine MSI controller driver
	broadcom/soc
		http://github.com/Broadcom/stblinux tags/arm-soc/for-4.6/soc
	broadcom/maintainers
		http://github.com/Broadcom/stblinux tags/arm-soc/for-4.6/maintainers

next/arm64
	patch
		arm64: defconfig: add spmi and usb related configs
	<no branch> (c840f28bbf643c361c463bcb8fb815d0b2dad4e8)
		git://git.kernel.org/pub/scm/linux/kernel/git/mmind/linux-rockchip tags/v4.6-rockchip-soc64-1
	mvebu/arm64
		git://git.infradead.org/linux-mvebu tags/mvebu-arm64-4.6-1
		contains depends/irqchip-armada-370-xp
	patch
		arm64: add Alpine SoC family
		arm64: defconfig: enable the Alpine family
		arm64: alpine: select the Alpine MSI controller driver
	broadcom/maintainers64
		http://github.com/Broadcom/stblinux tags/arm-soc/for-4.6/maintainers-arm64
	broadcom/defconfig-arm64
		http://github.com/Broadcom/stblinux tags/arm-soc/for-4.6/defconfig-arm64
	broadcom/soc-arm64
		http://github.com/Broadcom/stblinux tags/arm-soc/for-4.6/soc-arm64

next/dt
	mvebu/dt
		git://git.infradead.org/linux-mvebu tags/mvebu-dt-4.6-1
	patch
		ARM: dts: spear: replace gpio-key,wakeup with wakeup-source property
	lpc32xx/dt
		https://github.com/vzapolskiy/linux lpc32xx/dt
	vexpress/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/sudeep.holla/linux tags/vexpress-for-v4.6/dt-updates
	stm32/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/mcoquelin/stm32 tags/stm32-dt-for-v4.6-1
	rockchip/dts32
		git://git.kernel.org/pub/scm/linux/kernel/git/mmind/linux-rockchip tags/v4.6-rockchip-dts32-1
	patch
		ARM: add device-tree SoC bindings for Axis Artpec-6
		ARM: dts: artpeg: add Artpec-6 SoC dtsi file
		ARM: dts: artpec: add Artpec-6 development board dts
	renesas/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/horms/renesas tags/renesas-dt-for-v4.6
	samsung/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/krzk/linux tags/samsung-dt-4.6
	versatile/dt-cleanup
		git://git.kernel.org/pub/scm/linux/kernel/git/linusw/linux-integrator tags/versatile-dt-cleanup-1
	patch
		dts/ls2080a: Update PCIe compatible
	at91/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/abelloni/linux tags/at91-ab-4.6-dt
	mediatek/dts
		https://github.com/mbgg/linux-mediatek tags/v4.5-next-dts
	omap/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/tmlind/linux-omap tags/omap-for-v4.6/dt-pt1
	keystone/dts
		git://git.kernel.org/pub/scm/linux/kernel/git/ssantosh/linux-keystone tags/keystone_dts_for_4.6_v2
	zynq/dts
		https://github.com/Xilinx/linux-xlnx tags/zynq-dt-for-4.6
	patch
		ARM: dts: alpine: add the MSIX node
	broadcom/dt
		http://github.com/Broadcom/stblinux tags/arm-soc/for-4.6/devicetree

next/dt64
	patch
		MAINTAINERS: Adding Maintainers for AMD Seattle Device Tree
		dtb: amd: Fix GICv2 hypervisor and virtual interface sizes
		dtb: amd: Fix DMA ranges in device tree
		dtb: amd: Fix typo in SPI device nodes
		dtb: amd: Misc changes for I2C device nodes
		dtb: amd: Misc changes for SATA device tree nodes
		dtb: amd: Misc changes for GPIO devices
		dtb: amd: Add PERF CCN-504 device tree node
		dtb: amd: Add KCS device tree node
		dtb: amd: Add AMD XGBE device tree file
		dtb: amd: Add support for new AMD Overdrive boards
		dtb: amd: Add support for AMD/Linaro 96Boards Enterprise Edition Server board
	renesas/dt64
		git://git.kernel.org/pub/scm/linux/kernel/git/horms/renesas tags/renesas-arm64-dt-for-v4.6
	rockchip/dts64
		git://git.kernel.org/pub/scm/linux/kernel/git/mmind/linux-rockchip tags/v4.6-rockchip-dts64-1
	patch
		arm64: dts: amd: Fix-up for ccn504 and kcs nodes
	mvebu/dt64
		git://git.infradead.org/linux-mvebu tags/mvebu-dt64-4.6-1
	mediatek/dts64
		https://github.com/mbgg/linux-mediatek tags/v4.5-next-dts64
	zynq/dts64
		https://github.com/Xilinx/linux-xlnx tags/zynqmp-dt-for-4.6
	patch
		arm64: dts: add the Alpine v2 EVP
		arm64: dts: alpine: add the MSIX node in the Alpine v2 dtsi
	broadcom/dt-arm64
		http://github.com/Broadcom/stblinux tags/arm-soc/for-4.6/devicetree-arm64

next/defconfig
	renesas/defconfig
		git://git.kernel.org/pub/scm/linux/kernel/git/horms/renesas tags/renesas-defconfig-for-v4.6
	mvebu/defconfig
		git://git.infradead.org/linux-mvebu tags/mvebu-defconfig-4.6-1
	integrator/defconfig
		git://git.kernel.org/pub/scm/linux/kernel/git/linusw/linux-integrator tags/realview-defconfig-redux
	patch
		ARM: multi_v7_defconfig: add MACH_ARTPEC6
	samsung/defconfig
		git://git.kernel.org/pub/scm/linux/kernel/git/krzk/linux tags/samsung-defconfig-4.6
		contains depends/rtc-max77802
	at91/defconfig
		git://git.kernel.org/pub/scm/linux/kernel/git/abelloni/linux tags/at91-ab-4.6-defconfig
	socfpga/defconfig
		git://git.kernel.org/pub/scm/linux/kernel/git/dinguyen/linux tags/socfpga_defconfig_for_v4.6
	patch
		ARM: multi_v7_defconfig: enable useful configurations for Vybrid
		ARM: multi_v5_defconfig: Cleanup multi_v5_defconfig
		ARM: multi_v5_defconfig: Enable initramfs support
	broadcom/defconfig
		http://github.com/Broadcom/stblinux tags/arm-soc/for-4.6/defconfig

next/drivers
	rockchip/drivers
		git://git.kernel.org/pub/scm/linux/kernel/git/mmind/linux-rockchip tags/v4.6-rockchip-drivers1
	samsung/drivers
		git://git.kernel.org/pub/scm/linux/kernel/git/krzk/linux tags/samsung-drivers-4.6
	scpi/updates
		git://git.kernel.org/pub/scm/linux/kernel/git/sudeep.holla/linux tags/scpi-for-v4.6/updates
	at91/drivers
		git://git.kernel.org/pub/scm/linux/kernel/git/abelloni/linux tags/at91-ab-4.6-drivers
	drivers/reset
		git://git.pengutronix.de/git/pza/linux tags/reset-for-4.6
